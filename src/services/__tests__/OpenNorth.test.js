import { getRepresentativesBy } from '../OpenNorth';
import mock_representatives from '../respresentatives';
import fetchJsonp from 'fetch-jsonp';

jest.mock('fetch-jsonp', () => jest.fn(() => new Promise()));

describe('OpenNorth', async () => {
  it('should call fetchJsonp to retrieve a list of representatives', () => {
    const postalCode = 'T2Z4M8';
    fetchJsonp.mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mock_representatives)
      })
    );
    const mps = getRepresentativesBy(postalCode);

    mps.then(json => {
      expect(fetchJsonp).toBeCalled();
      expect(json[postalCode]).toBe(mock_representatives);
    });
  });

  it('should catch an error if there was a problem', () => {
    fetchJsonp.mockImplementation(() => Promise.reject('Mock error'));
    const mps = getRepresentativesBy('T2Z4M8');
    mps.catch(e => expect(e.message).toBe('There was an error: Mock error'));
  });
});
