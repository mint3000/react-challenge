export default {
  province: 'AB',
  code: 'T2Z4M8',
  boundaries_concordance: [],
  representatives_centroid: [
    {
      extra: {},
      photo_url:
        'http://www.ourcommons.ca/Parliamentarians/Images/OfficialMPPhotos/42/KmiecTom_CPC.jpg',
      source_url:
        'http://www.parl.gc.ca/Parliamentarians/en/members?view=ListAll',
      elected_office: 'MP',
      last_name: 'Kmiec',
      email: 'Tom.Kmiec@parl.gc.ca',
      district_name: 'Calgary Shepard',
      personal_url: 'http://www.tomkmiecmp.ca',
      related: {
        boundary_url: '/boundaries/federal-electoral-districts/48011/',
        representative_set_url: '/representative-sets/house-of-commons/'
      },
      first_name: 'Tom',
      gender: '',
      representative_set_name: 'House of Commons',
      name: 'Tom Kmiec',
      party_name: 'Conservative',
      url: 'http://www.parl.gc.ca/Parliamentarians/en/members/Tom-Kmiec(89136)',
      offices: [
        {
          fax: '1 613 992-0883',
          tel: '1 613 992-0846',
          type: 'legislature',
          postal: 'House of Commons\nOttawa ON  K1A 0A6'
        },
        {
          tel: '1 403 974-1285',
          type: 'constituency',
          postal: '3114 Glenmore Court SE (Main Office)\n\nCalgary AB  T2C 2E6'
        }
      ]
    }
  ],
  city: 'Calgary',
  boundaries_centroid: [
    {
      name: 'Calgary-Peigan',
      boundary_set_name: 'Alberta electoral district',
      related: {
        boundary_set_url: '/boundary-sets/alberta-electoral-districts-2017/'
      },
      url: '/boundaries/alberta-electoral-districts-2017/calgary-peigan/',
      external_id: '22'
    },
    {
      name: 'Calgary Shepard',
      boundary_set_name: 'Federal electoral district',
      related: {
        boundary_set_url: '/boundary-sets/federal-electoral-districts/'
      },
      url: '/boundaries/federal-electoral-districts/48011/',
      external_id: '48011'
    },
    {
      name: 'Calgary-Fort',
      boundary_set_name: 'Alberta electoral district',
      related: {
        boundary_set_url: '/boundary-sets/alberta-electoral-districts/'
      },
      url: '/boundaries/alberta-electoral-districts/calgary-fort/',
      external_id: '12'
    },
    {
      name: 'Ward 12',
      boundary_set_name: 'Calgary ward',
      related: {
        boundary_set_url: '/boundary-sets/calgary-wards/'
      },
      url: '/boundaries/calgary-wards/ward-12/',
      external_id: '12'
    },
    {
      name: 'Calgary Southeast',
      boundary_set_name: 'Federal electoral district',
      related: {
        boundary_set_url:
          '/boundary-sets/federal-electoral-districts-2003-representation-order/'
      },
      url:
        '/boundaries/federal-electoral-districts-2003-representation-order/48007/',
      external_id: '48007'
    },
    {
      name: 'Division No.  6',
      boundary_set_name: 'Census division',
      related: {
        boundary_set_url: '/boundary-sets/census-divisions/'
      },
      url: '/boundaries/census-divisions/4806/',
      external_id: '4806'
    },
    {
      name: 'Calgary',
      boundary_set_name: 'Census subdivision',
      related: {
        boundary_set_url: '/boundary-sets/census-subdivisions/'
      },
      url: '/boundaries/census-subdivisions/4806016/',
      external_id: '4806016'
    }
  ],
  centroid: {
    type: 'Point',
    coordinates: [-113.976992, 50.945127]
  }
};
