import fetchJsonp from 'fetch-jsonp';

const url = 'https://represent.opennorth.ca/postcodes/';

/**
 * Fetches representatives by postal code
 *
 * @param {*} postalCode
 */

let cache = [];
export async function getRepresentativesBy(postalCode) {
  postalCode = postalCode.match(' ') ? postalCode.replace(' ', '') : postalCode;
  try {
    if (typeof cache[postalCode] === 'undefined') {
      const mps = await fetchJsonp(`${url}${postalCode.toUpperCase()}`);
      cache[postalCode] = [];
      mps.json().then(response => (cache[postalCode] = response));
    }
    return cache;
  } catch (e) {
    throw new Error(`There was an error: ${e}`);
  }
}
