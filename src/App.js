import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import PostalCodeInput from './components/PostalCodeInput';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Find your MP</h1>
        </header>
        <div className="container">
          <PostalCodeInput />
        </div>
      </div>
    );
  }
}

export default App;
