import React, { Component } from 'react';
import { getRepresentativesBy } from '../services/OpenNorth';
import MPCard from './MPCard';

class PostalCodeInput extends Component {
  state = {
    error: false,
    errorMessage: '',
    hasResult: false,
    postalCode: '',
    results: []
  };

  handleInputChange = e =>
    this.setState({
      postalCode: e.currentTarget.value
    });

  handleFormSubmit = e => {
    e.preventDefault();
    const { postalCode } = this.state;
    const validPostalCode = this.validate(postalCode);
    if (validPostalCode) {
      const mps = getRepresentativesBy(postalCode);
      mps
        .then(response =>
          this.setState({
            error: false,
            hasResult: true,
            results: {
              ...response[postalCode],
              representatives_centroid: this.filter(response[postalCode])
            }
          })
        )
        .catch(err => {
          this.setState({
            error: true,
            errorMessage: `${err}`,
            hasResult: false
          });
        });
    } else {
      this.setState({
        error: true,
        errorMessage: 'Please use a valid postal code',
        hasResult: false
      });
    }
  };

  /**
   * Filters results for MP's
   *
   * @param {Array} results
   *  Array of results from API
   */
  filter = results => {
    if (results.representatives_centroid.length === 0) {
      throw new Error(`No MP's found for ${this.state.postalCode}`);
    }
    return results.representatives_centroid.filter(
      mp => mp.elected_office === 'MP'
    );
  };

  /**
   * Validate a postal code
   *
   * @param {String} postalCode
   */
  validate = postalCode => {
    const regex = /^[A-Za-z]\d[A-Za-z][ ]?\d[A-Za-z]\d$/;
    return regex.exec(postalCode);
  };

  render() {
    return (
      <div className="form-wrapper">
        <form
          data-testid="searchPostalCodeForm"
          onSubmit={this.handleFormSubmit}
        >
          <label>Enter your postal code to find your MP!</label>
          <input
            data-testid="postalCode"
            type="text"
            value={this.state.postalCode}
            onChange={this.handleInputChange}
            name="postalCode"
          />
          <input data-testid="submitButton" type="submit" value="Go" />
        </form>
        {this.state.error && (
          <div className="error-message">{this.state.errorMessage}</div>
        )}
        {this.state.hasResult && <MPCard mps={this.state.results} />}
      </div>
    );
  }
}

export default PostalCodeInput;
