import React from 'react';

const MPCard = props => {
  return (
    <div className="mp-wrapper">
      {props.mps.representatives_centroid.map((mp, index) => (
        <div data-testid="mp-info-card" className="mp-info-card" key={index}>
          <div className="mp-stats">
            <div className="mp-stats_image-wrapper">
              <img src={mp.photo_url} alt={`${mp.name}`} />
            </div>
            <div className="mp-stats_info-wrapper">
              <h2>
                {mp.name}
                <br />
                <small>{mp.representative_set_name}</small>{' '}
              </h2>
              <p>
                <strong>Province:</strong> {props.mps.province}
                <br />
                <strong>District:</strong> {mp.district_name}
                <br />
                <strong>Party:</strong> {mp.party_name}
              </p>
              <p>
                <strong>Email:</strong>
                <br />
                <a href={`mailto:${mp.email}`}>{mp.email}</a>
                <br />
                <strong>Web:</strong>
                <br />
                <a href={mp.personal_url}>{mp.personal_url}</a>
              </p>
            </div>
            <div className="mp-stats_contact-wrapper">
              <p>
                <strong>Address:</strong>
              </p>
              {mp.offices.map((office, index) => (
                <div key={index}>
                  <p>{office.postal}</p>
                  <ul>
                    <li>Tel: {office.tel}</li>
                    {office.fax && <li>Fax: {office.fax}</li>}
                  </ul>
                </div>
              ))}
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default MPCard;
