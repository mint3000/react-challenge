import React from 'react';
import { shallow } from '../../enzyme';
import PostalCodeInput from '../PostalCodeInput';
import { getRepresentativesBy } from '../../services/OpenNorth';
import mock_representatives from '../../services/respresentatives';

jest.mock('../../services/OpenNorth');

/**
 * Simulates inputting values
 *
 * @param {*} wrapper
 * @param {*} instance
 * @param {*} newValue
 */
const updateInput = (wrapper, instance, newValue) => {
  const input = wrapper.find(instance);
  input.simulate('change', { currentTarget: { value: `${newValue}` } });
  return wrapper.find(instance);
};

const validPostalCode = 'T2A1H6';
const validPostalCodeWithSpace = 'T2A 1H6';
const invalidPostalCode = 'postal';

describe('PostalCodeInput', () => {
  // Enter postal code
  it('should allow a user to fill out the form', () => {
    const wrapper = shallow(<PostalCodeInput />);
    const postalCode = updateInput(
      wrapper,
      '[data-testid="postalCode"]',
      validPostalCode
    );

    expect(postalCode.props().value).toBe(validPostalCode);
  });

  // validate Postal code
  it('should handle a valid postal code', () => {
    const wrapper = shallow(<PostalCodeInput />);
    jest.spyOn(wrapper.instance(), 'validate');
    getRepresentativesBy.mockImplementation(() =>
      Promise.resolve({
        json: () => mock_representatives
      })
    );

    updateInput(wrapper, '[data-testid="postalCode"]', validPostalCode);
    wrapper
      .find('[data-testid="searchPostalCodeForm"]')
      .simulate('submit', { preventDefault: () => {} });

    // Validate with no space;
    expect(wrapper.instance().validate.mock.calls.length).toBe(1);
    expect(wrapper.instance().validate).toHaveBeenCalledWith(validPostalCode);
    expect(wrapper.instance().validate(validPostalCode)).toBeTruthy();

    updateInput(
      wrapper,
      '[data-testid="postalCode"]',
      validPostalCodeWithSpace
    );
    wrapper
      .find('[data-testid="searchPostalCodeForm"]')
      .simulate('submit', { preventDefault: () => {} });

    // Validate with a space
    expect(wrapper.instance().validate).toHaveBeenCalledWith(
      validPostalCodeWithSpace
    );
    expect(wrapper.instance().validate(validPostalCodeWithSpace)).toBeTruthy();
  });

  it('should handle a invalid postal code', () => {
    const wrapper = shallow(<PostalCodeInput />);
    jest.spyOn(wrapper.instance(), 'validate');
    updateInput(wrapper, '[data-testid="postalCode"]', invalidPostalCode);
    wrapper
      .find('[data-testid="searchPostalCodeForm"]')
      .simulate('submit', { preventDefault: () => {} });

    expect(wrapper.instance().validate.mock.calls.length).toBe(1);
    expect(wrapper.instance().validate).toHaveBeenCalledWith(invalidPostalCode);
    expect(wrapper.instance().validate(invalidPostalCode)).toBeNull();
  });

  // submit form, call api
  it('submits the form', () => {
    const wrapper = shallow(<PostalCodeInput />);
    getRepresentativesBy.mockImplementation(() =>
      Promise.resolve({
        json: () => mock_representatives
      })
    );
    updateInput(wrapper, '[data-testid="postalCode"]', validPostalCode);
    wrapper
      .find('[data-testid="searchPostalCodeForm"]')
      .simulate('submit', { preventDefault: () => {} });
    expect(getRepresentativesBy).toHaveBeenCalledWith(validPostalCode);
  });
});
