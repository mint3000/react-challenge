import React from 'react';
import { shallow } from '../../enzyme';
import MPCard from '../MPCard';
import mock_respresentatives from '../../services/respresentatives';
import toJson from 'enzyme-to-json';

describe('MPCard', () => {
  it('should display a card', () => {
    const wrapper = shallow(<MPCard mps={mock_respresentatives} />);
    const card = wrapper.find('[data-testid="mp-info-card"]');
    expect(card.length).toBe(1);
  });

  it('should match the snapshot', () => {
    const wrapper = shallow(<MPCard mps={mock_respresentatives} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
